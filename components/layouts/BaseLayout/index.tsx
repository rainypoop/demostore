import { FunctionComponent, ReactNode } from 'react'

const BaseLayout: FunctionComponent<{ children?: ReactNode }> = ({ children }) => {
  return (
    <main className='flex min-h-screen justify-center transition-all duration-200'>
      {children}
    </main>
  )
}

export default BaseLayout
