import { NextPage } from 'next'
import type { AppProps } from 'next/app'
import Head from 'next/head'
import { ReactElement, ReactNode } from 'react'
import BaseLayout from '../components/layouts/BaseLayout'
import '../styles/globals.css'

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ??
    ((page) => <BaseLayout>{page}</BaseLayout>)

  return (
    <>
      <Head>
        <title>Demo Store</title>
      </Head>
      {getLayout(<Component {...pageProps} />)}
    </>
  )
}

export default MyApp
