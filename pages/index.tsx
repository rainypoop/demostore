import { NextPageWithLayout } from './_app'

const Home: NextPageWithLayout = () => {
  return (
    <div>
      Home Page
    </div>
  )
}

export default Home
